import { NewFollowComponent } from './new-follow/new-follow.component';
import { NewObservationComponent } from './new-observation/new-observation.component';
import { NewEntryComponent } from './new-entry/new-entry.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { LandingComponent } from './landing/landing.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: "", component: LandingComponent},
  // {path: "ressources", component: },
  // {path: "infos", component: },
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},  
  {path: "logout", component: LogoutComponent},
  {path: "home", component: HomeComponent},
  {path: "new-entry/:id", component: NewEntryComponent},
  {path: "new-observation", component: NewObservationComponent},
  {path: "new-follow", component: NewFollowComponent},


  { path: '404', component: NotFoundComponent},
  { path: '**', redirectTo: '/404'},    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
