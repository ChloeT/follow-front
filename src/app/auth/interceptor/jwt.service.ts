import { AuthService } from './../repository/auth.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtService implements HttpInterceptor {

  constructor(private authService:AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.authService.getToken()) {
      req = req.clone({
        setHeaders : {
          Authorization: 'Bearer '+this.authService.getToken()
        }
      });
    }
    return next.handle(req);
  }
}
