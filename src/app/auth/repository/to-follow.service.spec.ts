import { TestBed } from '@angular/core/testing';

import { ToFollowService } from './to-follow.service';

describe('ToFollowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToFollowService = TestBed.get(ToFollowService);
    expect(service).toBeTruthy();
  });
});
