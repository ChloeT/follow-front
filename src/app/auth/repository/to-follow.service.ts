import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { ToFollow } from './../entity/toFollow';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToFollowService {

  private url = 'http://localhost:8000/api';

  public follow:BehaviorSubject<ToFollow> = new BehaviorSubject(null);


  constructor(private http:HttpClient) {}
  

  addNew(follow:ToFollow) {
    return this.http.post<ToFollow>(this.url+'/follow', follow);
  }

  // findAll(): Observable<ToFollow[]> {
  //   return this.http.get<ToFollow[]>(this.url+'/follow');
  // }

  
  getById(id): Observable<ToFollow> {
    return this.http.get<ToFollow>(this.url+'/follow/'+id).pipe(
      tap(follow => this.follow.next(follow))
    );
  }


}