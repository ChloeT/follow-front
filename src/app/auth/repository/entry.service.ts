import { Entry } from './../entity/entry';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  private url = 'http://localhost:8000/api';

  public entry: BehaviorSubject<Entry> = new BehaviorSubject(null);


  constructor(private http: HttpClient) { }


  addNew(entry: Entry) {
    if (entry.follow || entry.observation) {
      return this.http.post<Entry>(this.url + '/entry', entry);
    }
  }


  getByCurentUser(): Observable<Entry[]> {
    return this.http.get<Entry[]>(this.url+'/entry/');
  }
}
