import { Entry } from './entry';
import { ToFollow } from './toFollow';
export interface User{
    id?:number;
    email:string;
    password?:string;
    roles?:string[];
    followed?:ToFollow[],
    entries?: Entry[]
}
