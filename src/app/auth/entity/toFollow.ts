import { User } from './user';
export interface ToFollow{
    id?:number;
    name: string;
    iconSlug: string;
    isIntensity: boolean;
    user?: User;
    // occurance: Occurances[];
}