import { ToFollow } from './toFollow';
import { User } from './user';
export interface Entry {
    id?: number;
    intensity?: number;
    observation?: string;
    date?: Date;
    user?: User;
    follow?: ToFollow;
    followId?: number;
}