import { Router } from '@angular/router';
import { User } from './../entity/user';
import { AuthService } from './../repository/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = {
    email: null,
    password: null
  };
  repeat: string = null;
  message: string;
  currentUser: User

  constructor(private authRepo: AuthService, private router: Router) { }

  ngOnInit() {
    this.authRepo.user.subscribe(user => this.currentUser = user);
  }

  register() {
    if (this.validForm()) {
      this.authRepo.addUser(this.user).subscribe(() => this.message = 'successfully registered !',
        data => this.message = data.error.message);
      this.router.navigate(['/login']);
    } else {
      this.message = 'Passwords does not match';
    }
  }

  validForm(): boolean {
    return this.user.email && this.user.password && this.user.password === this.repeat;
  }

  goTo(route: string) {
    this.router.navigate(['/' + route]);
  }

  logout() {
    this.authRepo.logout();
    this.message = '';
  }
}
