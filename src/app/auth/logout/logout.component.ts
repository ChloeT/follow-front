import { AuthService } from './../repository/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private authRepo:AuthService, private router:Router) { }

  ngOnInit() {
    this.authRepo.logout();
    this.router.navigate(['']);
  }

}
