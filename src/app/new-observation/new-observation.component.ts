import { Location } from '@angular/common';
import { EntryService } from './../auth/repository/entry.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Entry } from '../auth/entity/entry';

@Component({
  selector: 'app-new-observation',
  templateUrl: './new-observation.component.html',
  styleUrls: ['./new-observation.component.css']
})
export class NewObservationComponent implements OnInit {

  entry: Entry = {
    intensity: null,
    observation: null,
    follow: null,
  };
  message: string;


  constructor(
    private repoEntry: EntryService,
    private router: Router,
    private location: Location,

  ) { }


  ngOnInit(): void {

  }


  newEntry() {
    if (this.entry.follow || this.entry.observation) {
    this.repoEntry.addNew(this.entry).subscribe(() => this.message = 'successfully created !',
      data => this.message = data.error.message);
    this.router.navigate(['/home']);
    }else{this.message='please add something'}
  }

  goBack(): void {
    this.location.back();
  }

}





