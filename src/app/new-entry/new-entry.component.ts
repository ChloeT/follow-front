import { EntryService } from './../auth/repository/entry.service';
import { Entry } from './../auth/entity/entry';
import { ToFollowService } from './../auth/repository/to-follow.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-new-entry',
  templateUrl: './new-entry.component.html',
  styleUrls: ['./new-entry.component.css']
})

export class NewEntryComponent implements OnInit {

  entry: Entry = {
    intensity: null,
    observation: null,
    follow: null,
  };
  message: string;


  constructor(
    private repoFollow: ToFollowService,
    private activatedRoute: ActivatedRoute,
    private repoEntry: EntryService,
    private router: Router,
    private location: Location,


  ) { }


  ngOnInit(): void {

    this.getFollow();
  }

  getFollow(): void {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.repoFollow.getById(id)
      .subscribe(follow => this.entry.follow = follow);
  }


  intensityPlus(): void {

    if (!this.entry.intensity) {
      this.entry.intensity = 1
    } else {
      this.entry.intensity++
    }
  }

  intensityLess(): void {
    
    if (!this.entry.intensity) {
      this.entry.intensity = 5
    } else {
      this.entry.intensity--
    }
  }


  newEntry() {
    this.entry.followId = this.entry.follow.id;

    this.repoEntry.addNew(this.entry).subscribe(() => this.message = 'successfully created !',
      data => this.message = data.error.message);

      this.router.navigate(['/home']);
    
  }

  goBack(): void {
    this.location.back();
  }

}





