import { AddEntryComponent } from './home/add-entry/add-entry.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { JwtService } from './auth/interceptor/jwt.service';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { LandingComponent } from './landing/landing.component';
import { HomeComponent } from './home/home.component';
import { JournalComponent } from './home/journal/journal.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NewEntryComponent } from './new-entry/new-entry.component';
import { NewObservationComponent } from './new-observation/new-observation.component';
import { NewFollowComponent } from './new-follow/new-follow.component';
import { LogoComponent } from './logo/logo.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    LandingComponent,
    HomeComponent,
    AddEntryComponent,
    JournalComponent,
    NotFoundComponent,
    NewEntryComponent,
    NewObservationComponent,
    NewFollowComponent,
    LogoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
