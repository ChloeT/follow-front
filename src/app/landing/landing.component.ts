import { Router } from '@angular/router';
import { AuthService } from '../auth/repository/auth.service';
import { User } from '../auth/entity/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  currentUser: User;

  constructor(private repoAuth: AuthService, private router: Router) { }

  ngOnInit() {

    this.repoAuth.user.subscribe(user => this.currentUser = user);

  }

  goTo(route:string){
    this.router.navigate(['/'+route]);
  }
}
