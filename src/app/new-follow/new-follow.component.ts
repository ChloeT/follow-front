import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ToFollowService } from './../auth/repository/to-follow.service';
import { ToFollow } from './../auth/entity/toFollow';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-follow',
  templateUrl: './new-follow.component.html',
  styleUrls: ['./new-follow.component.css']
})
export class NewFollowComponent implements OnInit {


  icons: any[] = [
    { "code": "fas fa-bolt" },
    { "code": "fas fa-tint" },
    { "code": "fas fa-sad-tear" },

  ]

  follow: ToFollow = {
    name: null,
    iconSlug: "fas fa-circle-notch",
    isIntensity: false,
  };

  message: string;

  // followNameLength: number;



  constructor(
    private repoFollow: ToFollowService,
    private router: Router,
    private location: Location,
  ) { }



  ngOnInit() {
  }



  newFollow() {
    if (this.isValid(this.follow)) {

      this.repoFollow.addNew(this.follow).subscribe(() => this.message = 'successfully created !',
        data => this.message = data.error.message);

        this.router.navigate(['/home']);
    }
  }

  isValid(follow: ToFollow) {
    if (this.follow.name) {
      return true
    } else {
      this.message = 'please complete all fields'
      return false
    }
  }

  goBack(): void {
    this.location.back();
  }

  // followNameLength = this.follow.name.length();


}
