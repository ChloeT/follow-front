import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFollowComponent } from './new-follow.component';

describe('NewFollowComponent', () => {
  let component: NewFollowComponent;
  let fixture: ComponentFixture<NewFollowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFollowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFollowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
