import { Entry } from './../../auth/entity/entry';
import { EntryService } from './../../auth/repository/entry.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-journal',
  templateUrl: './journal.component.html',
  styleUrls: ['./journal.component.css']
})
export class JournalComponent implements OnInit {
  
  entries: Entry[];

  constructor(private repoEntry: EntryService) { }

  ngOnInit() {
    this.repoEntry.getByCurentUser().subscribe(entries=> this.entries = entries);
  }


}
