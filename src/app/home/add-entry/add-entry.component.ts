import { User } from './../../auth/entity/user';
import { AuthService } from './../../auth/repository/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-entry',
  templateUrl: './add-entry.component.html',
  styleUrls: ['./add-entry.component.css']
})
export class AddEntryComponent implements OnInit {

  currentUser: User;

  
  constructor(private  repoAuth: AuthService ) { }

  ngOnInit() {
    this.repoAuth.user.subscribe(user => this.currentUser = user);
  }
  
 


}
