import { AuthService } from './../auth/repository/auth.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../auth/entity/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  shownComponent:string ='journal';

  constructor(
    private router: Router,
    private repoAuth : AuthService
    ) { }

  ngOnInit() {this.repoAuth.getUser().subscribe();

  }

  switch(toShow : string){
    this.shownComponent = toShow;
  }

  goTo(route:string){
    this.router.navigate(['/'+route]);
  }

}
