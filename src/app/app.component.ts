import { AuthService } from './auth/repository/auth.service';
import { User } from './auth/entity/user';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'follow-ng';


  constructor(private repoAuth:AuthService) { }

  ngOnInit() {
    this.repoAuth.getUser().subscribe();
  
  }
}
